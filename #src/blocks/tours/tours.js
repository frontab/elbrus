toursBoxInit();
$(window).on('resize', function() {
	toursBoxInit();
});
function toursBoxInit() {
	if ( $(window).width() < 1200 ) {

		if ( !$('.tours-box.active').length ) {
			$('.tours-box:first-child').addClass('active');
		}
	}
}

toursBoxSliderInit();
$(window).on('resize', function() {
	toursBoxSliderInit();
});
function toursBoxSliderInit() {
	if ( $(window).width() < 768 ) {

		if ( !$('.tours-box__list').hasClass('sldier-mobile') ) {

			$('.tours-box__list .card').wrap('<div class="tours-box__slide"></div>');

			$('.tours-box__list').slick({
				dots: false,
				arrows: false,
				infinite: true,
				slidesToShow: 1,
				slidesToScroll: 1,
				centerMode: true,
				variableWidth: true
			}).addClass('sldier-mobile');
		}

	} else {
		if ( $('.tours-box__list').hasClass('slick-initialized') ) {
			$('.tours-box__list').slick('unslick')
				.removeClass('sldier-mobile');
				$('.tours-box__list .card').unwrap();
		}
	}
}