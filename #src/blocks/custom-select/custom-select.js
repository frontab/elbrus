function customSelect() {

	$('.custom-select').each(function() {
		var itemSelect = $(this).find(".selected"),
			textArea = $(this).find('.custom-select__select-text'),
			placeholder = textArea.attr('data-placeholder') || "Выберите...";

		if ( itemSelect.length ) {
			textArea.html(itemSelect.html());
		} else {
			textArea.addClass('placeholder')
				.html(placeholder);
		}
	});
}
customSelect();

$('.custom-select li').on('click', function(e) {
	e.preventDefault();

	$(this).addClass('selected')
		.siblings()
		.removeClass('selected');

	$(this).closest('.custom-select')
		.toggleClass('opened')
		.find('.custom-select__select-text')
		.removeClass('placeholder')
		.html($(this).html());
});

$('.custom-select__select').on('click', function(e) {
	e.preventDefault();

	$(this).closest('.custom-select')
		.toggleClass('opened');
});

$(document).mouseup(function (e) {
	var boxArea = $('.custom-select');
	if (boxArea.has(e.target).length === 0) {
		boxArea.removeClass('opened');
	}
});

$('.custom-select__dropdown ul').scrollbar();