$('.counter a').on('click', function(e) {
	e.preventDefault();
	var btnType = $(this).attr('data-btn'),
		 span = $(this).closest('.counter')
							 .find('span'),
		 spanVal = span.text();
	if (btnType === 'dec') {
	  spanVal --;
	  if (spanVal <= 0) {
		 spanVal = 0;
	  }
	  span.text(spanVal);
	}
	if (btnType === 'inc') {
	  spanVal ++;
	  span.text(spanVal);
	}
 });