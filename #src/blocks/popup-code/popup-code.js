$('.popup-code__form .form__input').on('input', function(e) {
	e.preventDefault();

	if ( $(this).val().length > 4 ) {
		$(this).closest('.popup-code__form')
			.find('.btn.disabled')
			.removeClass('disabled');
	}
});

$('.popup-code__form .btn').on('click', function(e) {
	e.preventDefault();

	$('.order-steps__box.active').find('.btn.hidden')
		.removeClass('hidden')
		.prev('.btn')
		.addClass('hidden');
	
		$.fancybox.close();
});