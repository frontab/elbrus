$('.filter__btn').on('click', function(e) {
	e.preventDefault();

	$(this).addClass('active')
		.siblings()
		.removeClass('active');

	var btnType = $(this).attr('data-type');

	if ( btnType == "lines" ) {
		$('.tours').addClass('is-line');
	} else {
		$('.tours').removeClass('is-line');
	}
});

$('.filter__select select').on('change', function() {

	var selectVal = $(this).val();

	$('.tours-box').each(function() {
		if ( $(this).attr('data-date') === selectVal ) {
			$(this).addClass('active')
				.siblings()
				.removeClass('active');
		}
	});
});


btnFilterInit();
$(window).on('resize', function() {
	btnFilterInit();
});
function btnFilterInit() {
	if ( $(window).width() < 1200 ) {
		$('.filter-btn').addClass('js-popup');
	} else {
		$('.filter-btn').removeClass('js-popup');
	}
}

$('.js-filter-trigger').on('click', function(e) {

	if ( $(window).width() < 1200 ) {
	} else {
		e.preventDefault();
	
		$('body').toggleClass('filter-open');
	
		$('.filter .form__select').styler('destroy');
		$('.filter .form__select').styler({
			onFormStyled: function() {
			  $('.jq-selectbox__dropdown ul').scrollbar();
			}
		});
	}
});

filterPopupInit();
$(window).on('resize', function() {
	filterPopupInit();
});
function filterPopupInit() {
	if ( $(window).width() < 1200 ) {
		// обернуть как попап
		if ( !$('.filter__main').hasClass('popup') ) {
			$('.filter__main').wrap('<div class="hidden"></div>');
			$('.filter__main').addClass('popup')
				.attr('id', 'popup-filter');
			$('.filter__main > *').wrapAll('<div class="popup__content"></div>');
			$('.filter__main .popup__content').prepend('<div class="popup__title">' + $('.filter__main').attr('data-title') + '</div>');
		}
	} else {
		// убрать обёртку
		if ( $('.filter__main').hasClass('popup') ) {
			$('.filter__main').unwrap();
			$('.filter__main').removeClass('popup fancybox-content')
				.attr('id', '')
				.find('.popup__title')
				.remove();
			$('.filter__main .filter__line').unwrap();
		}
	}
}