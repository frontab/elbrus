$('.lang__val').on('click', function(e) {
	e.preventDefault();

	$(this).closest('.lang')
		.toggleClass('active');
});

$('.lang__list a').on('click', function(e) {
	e.preventDefault();

	$(this).closest('li')
		.addClass('active')
		.siblings()
		.removeClass("active");

	$(this).closest('.lang')
		.removeClass('active');

	$(this).closest('.lang')
		.find('.lang__val span')
		.text( $(this).text() );
});

$(document).mouseup(function (e) {
	var boxArea = $('.lang');
	if (boxArea.has(e.target).length === 0) {
		boxArea.removeClass('active');
	}
});