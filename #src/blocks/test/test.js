$('.test-box__item').on('click', function(e) {
	e.preventDefault();

	$(this).addClass('active')
		.siblings()
		.removeClass('active');
	
	$(this).closest('.test-box')
		.addClass('answered');
	
	testProgressCheck( $(this).closest('.test') );

	if ( $(this).closest('.test-box').next().length ) {
		$(this).closest('.test-box')
			.removeClass('active')
			.next()
			.addClass('active');
			
		testStepCheck( $(this).closest('.test-box').next() );
	}

});


$('.test__back button').on('click', function(e) {
	e.preventDefault();

	if ( $(this).closest('.test').find('.test-box.active').prev().length ) {
		$(this).closest('.test')
			.find('.test-box.active')
			.removeClass('active')
			.prev()
			.addClass('active');

		testStepCheck( $(this).closest('.test').find('.test-box.active') );
	}
});

function testStepCheck(item) {
	item.closest('.test')
		.find('.test__top span')
		.text(item.index() + 1);
}

function testProgressCheck(item) {
	var allItems = item.find('.test-box').length,
		boxActiveIndex = item.find('.test-box.answered').length,
		perVal = boxActiveIndex / allItems * 100 + "%";
		
	item.find('.test__progress-title span')
		.text(perVal);
		
	item.find('.test__progress-line span')
		.css("width",perVal);
}

testBoxHeightCheck();
$(window).on('resize', function() {
	testBoxHeightCheck();
});
function testBoxHeightCheck() {
	$('.test').each(function() {
		var boxH = [];

		$(this).find('.test-box')
			.each(function() {
				$(this).css('min-height', 'auto');
				boxH.push( $(this).height() );
			});
		
		var newH = Math.max.apply( Math, boxH );

		$(this).find('.test-box')
			.each(function() {
				$(this).css('min-height', newH);
			});
	});
}