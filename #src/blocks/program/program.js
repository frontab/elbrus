$('.program__btn').on('click', function(e) {
	e.preventDefault();

	$(this).addClass('active')
		.siblings()
		.removeClass('active');

	$('.program__title').text( $(this).attr('data-title') );

	var btnIndex = $(this).index();

	$('.program__view').each(function() {
		if ( $(this).index() === btnIndex ) {
			$(this).addClass('active')
				.siblings()
				.removeClass('active');
		}
	});

	if ( $(this).hasClass('program__btn--height') ) {
		$('.program__main-wrap').addClass('scroll-mod');
	} else {
		$('.program__main-wrap').removeClass('scroll-mod');
	}

});

programScrollModInit();

$(window).on("resize", function() {
	programScrollModInit();
});
function programScrollModInit() {
	if ( $(window).width() < 992 ) {
		if ( $('.program__pics').length ) {
			var newW = $('.program__day').length * ( $('.program__day').width() + 1 ) + 72;
	
			$('.program__pics').css("width",newW);
	
			if ( $('.program__btn.active').index() === 0 ) {
				$('.program__main-wrap').addClass('scroll-mod');
			} else {
				$('.program__main-wrap').removeClass('scroll-mod');
			}
		}

		// переместить	
		if ( $('.program__height').length ) {
			$('.program__height').appendTo('.program__main-wrap');
		}
	} else {
		// на место	
		if ( $('.program__height').length ) {
			$('.program__height').appendTo('.program__view--diag');
		}
	}
}


$('.program__day').on('click', function(e) {
	e.preventDefault();

	$(this).addClass('active')
		.siblings()
		.removeClass('active');

	var dayIndex = $(this).index(),
		dayType= $(this).attr('data-type');

	if ( dayType === "snow" ) {
		$('.program__pic:first-child').removeClass('active');
		$('.program__pic:last-child').addClass('active');
		$('.program__line').addClass('program__line--snow');
	} else {
		$('.program__pic:last-child').removeClass('active');
		$('.program__pic:first-child').addClass('active');
		$('.program__line').removeClass('program__line--snow');
	}

	$('.program__img, .program__box, .program__step').each(function() {
		if ( $(this).index() === dayIndex ) {
			$(this).addClass('active')
				.siblings()
				.removeClass('active');
		}
	});
});

programHeightInit();

$(window).on("resize", function() {
	programHeightInit();
});

function programHeightInit() {
	if ( $('.program__height').length ) {

		var vewH = $('.program__height').height();
		
		$('.program__view--diag').css('height',vewH);
	}
}