$('.equipment-tour__code .btn').on('click', function(e) {
	e.preventDefault();

	$(this).closest('.equipment-tour__code')
		.toggleClass('active')
		.siblings('.equipment-tour__info')
		.toggleClass('active');
});

$('.equipment-tour__code .form__input').on('input', function() {
	if ( $(window).width() < 768 ) {
		if ( $(this).val().length ) {
			$(this).closest('.equipment-tour__code')
			.toggleClass('active')
			.siblings('.equipment-tour__info')
			.toggleClass('active');
		}
	}
});

$(document).on('click', '.equipment-basket__box', function(e) {
	e.preventDefault();
		$(this).closest('.equipment-basket')
			.toggleClass('active');
		
		$('.equipment-basket__items').toggleClass('active');

		$('body').toggleClass('equip-basket-open');
});

$(document).mouseup(function (e) {
	if ( $(window).width() < 768 ) {
		var boxArea = $('.equipment-basket');
		if (boxArea.has(e.target).length === 0) {
			boxArea.removeClass('active');
			$('body').removeClass('equip-basket-open');
		}
	}
});

$(document).on('click', '.equipment-tour__top', function(e) {
	e.preventDefault();

	if ( $(window).width() < 768 ) {
		$(this).toggleClass('active');
	}
});


$(document).on('mouseout', '.equipment-basket', function() {
	
	$('.equipment-basket__items').removeClass('active');
});

$(document).on('mouseover mousemove', '.equipment-basket.active', function() {

	$('.equipment-basket__items').addClass('active');
});

$('.equipment-basket__items-list').scrollbar();

$('.equipment-aside__box > a').on('click', function(e) {
	e.preventDefault();

	$(this).closest('.equipment-aside__box')
		.toggleClass('active')
		.siblings()
		.removeClass('active');

	if ( $(window).width() < 768 ) {
		var btnIndex = $(this).closest('.equipment-aside__box').index();

		$('.equipment-logs').each(function() {
			if ( $(this).index() === btnIndex ) {
				$(this).addClass('active')
					.siblings()
					.removeClass('active');
			}
		});
	}
});