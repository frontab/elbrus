sliderInit();
$(window).on('resize', function() {
	sliderInit();
});

function sliderInit() {
	if ( $(window).width() < 768 ) {
		if ( !$('.slider').hasClass('slider-mobile') ) {

			if ( $('.slider').hasClass('slick-initialized') ) {
				$('.slider').slick('unslick');
			}
	
			if ( $('.slider-box').length ) {
				$('.slider-box__item').unwrap();
			}
	
			// мобильный слайдер
			$('.slider').slick({
				dots: false,
				arrows: false,
				infinite: true,
				slidesToShow: 1,
				slidesToScroll: 1,
				centerMode: true,
				variableWidth: true
			}).addClass('slider-mobile')
				.removeClass('slider-desktop');
		}
		
	} else {
		if ( $('.slider').hasClass('slider-mobile') ) {

			if ( $('.slider').hasClass('slick-initialized') ) {
				$('.slider').slick('unslick');
			}
	
			if ( !$('.slider-box').length ) {
				// сгрупировать эллементы
				$('.slider').each(function() {
					var items = $(this).find('.slider-box__item'),
						itemsLength = items.length,
						combNum = parseInt($(this).attr('data-inner'));
					
					for ( var i = 0; i <= itemsLength; i = i + combNum ) {
						items.slice(i, i + combNum ).wrapAll('<div class="slider-box"></div>');
					}
				});
			}
		}

		if ( !$('.slider').hasClass('slider-desktop') ) {
			// основной слайдер
			$('.slider, .slider-view').on('init reInit', function(event, slick, currentSlide, nextSlide){
				$(this).closest('.slider-wrap')
						.find('.slider-counts span:last-child')
						.text( slick.slideCount );
			});
			
			$('.slider').slick({
				dots: false,
				arrows: false,
				infinite: false,
				slidesToShow: 1,
				slidesToScroll: 1
			}).removeClass('slider-mobile')
				.addClass('slider-desktop');
			
			$('.slider, .slider-view').on('afterChange', function(event, slick, currentSlide){
				$(this).closest('.slider-wrap')
						.find('.slider-counts span:first-child')
						.text( currentSlide + 1 );
			});
			
			$('.slider-btn--prev').on('click', function() {
				$(this).closest(".slider-wrap")
						.find('.slider, .slider-view')
						.slick('slickPrev');
			});
			$('.slider-btn--next').on('click', function() {
				$(this).closest(".slider-wrap")
						.find('.slider, .slider-view')
						.slick('slickNext');
			});
		}
	}
}