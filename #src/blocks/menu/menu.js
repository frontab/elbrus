$('.js-menu-toggle').on('click', function(e) {
	e.preventDefault();

	$('.menu').toggleClass('active');
});

$(document).mouseup(function (e) {
	var boxArea = $('.menu');
	var btnArea = $('.burger');
	if (boxArea.has(e.target).length === 0 && btnArea.has(e.target).length === 0) {
		boxArea.removeClass('active');
	}
});