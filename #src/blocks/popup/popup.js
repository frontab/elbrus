$(document).on('click', '.js-popup', function (e) {
	e.preventDefault();
	if ($('body').hasClass('fancybox-active')) {
		$('[data-fancybox-close]').trigger('click');
	}
	$.fancybox.open({
		src: $(this).attr('href'),
		hash: false,
		touch: false,
		afterShow: function() {
			testBoxHeightCheck();
			$('.popup .form__select').styler('destroy');
			$('.popup .form__select').styler({
				onFormStyled: function() {
				  $('.jq-selectbox__dropdown ul').scrollbar();
				}
			});
		}
	});
});

$('.popup-box-wrap').scrollbar();