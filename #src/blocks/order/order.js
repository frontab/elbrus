function orderBoxCheck(item) {
	var itemIndex = item.index(),
		title = $('.order-step__title-val'),
		steps = $('.order-step');

	item.addClass('active')
		.siblings()
		.removeClass('active');
			
	var newTitle = item.attr("data-title");
	title.text(newTitle);

	steps.each(function() {
		if ( $(this).index() === itemIndex ) {
			$(this).addClass('active')
				.siblings()
				.removeClass('active');
		}
	});

	if ( itemIndex + 1 > 2 ) {
		$('.order-step__callback').addClass('hidden');
	} else {
		$('.order-step__callback').removeClass('hidden');
	}
}

$('.order__btn').on('click', function(e) {
	e.preventDefault();

	var btnType = $(this).attr('data-type'),
		box = $(this).closest('.order-steps__box');

	if ( btnType === "prev" ) {
		orderBoxCheck(box.prev());
	}
	
	if ( btnType === "next" ) {
		orderBoxCheck(box.next());
	}
	
	packageScrollHeight();
});


function orderBtnCheck(item, val) {
	if ( val ) {
		item.closest('.order-steps__box')
			.find('.order__btn.disabled')
			.removeClass('disabled');
	}
}

$('.order-steps__box .form__check').on('change', function(e) {
	e.preventDefault();

	orderBtnCheck($(this), true);
});

$('.order-package__main').scrollbar();

packageScrollHeight();
$(window).on('resize', function() {
	packageScrollHeight();
});
function packageScrollHeight() {
	if ( $('.order-package__main').length ) {
		if ( $(window).width() < 768 ) {
			var wHeight = $(window).height(),
				top = $('.order-package__main').offset().top;
				bottom = 80,
				newHeight = wHeight - top - bottom;

				$('.order-package__main').css('max-height', newHeight);
		} else {
			$('.order-package__main').prop('styles', "");
		}
	}
}


$(window).on('resize scroll', function() {
	orderPackageActionInit();
});
orderPackageActionInit();
function orderPackageActionInit() {
	if ( $('.order-package__card-action').length ) {

		var scrollBoxH = $('.order-package__main').height();

		$('.order-package__card-action').css('margin-top', scrollBoxH);
	}
}


$('.order-users__more button').on('click', function(e) {
	e.preventDefault();

	var userCount = $('.order-user').length + 1;

	var newUser = `<div class="order-user">
							<div class="order-user__title">Участник ` + userCount + `</div>
							<div class="order-user__form">
							<label class="form__label"><span class="form__label-name">Фамилия и имя</span>
								<input class="form__input" type="text" name="" placeholder="Введите фамилию и имя">
							</label>
							<label class="form__label"><span class="form__label-name">E-mail</span>
								<input class="form__input" type="email" name="" placeholder="e-mail">
							</label>
							<label class="form__label"><span class="form__label-name">Телефон<span class="form__label-name-info">(используется для авторизации)</span></span>
								<input class="form__input" type="tel" name="" placeholder="+7">
							</label>
							</div>
						</div>`;
	$('.order-users__boxs').append(newUser);

	var newTab = `<button class="order-users__btn">Участник ` + userCount + `</button>`;
	$('.order-users__controls').append(newTab);

	phoneMaskInit();
});



$('.form__input--code').on('input', function(e) {
	e.preventDefault();

	if ( $(this).val().length > 3 ) {
		$(this).closest('.order-pay__code')
			.find('.btn')
			.removeClass('disabled');
	} else {
		if ( !$(this).hasClass('success') ) {
			$(this).closest('.order-pay__code')
				.find('.btn')
				.addClass('disabled');
		}
	}
});

$('.order-pay__code .btn').on('click', function(e) {
	e.preventDefault();

	$(this).closest('.order-pay__code')
		.find('.form__input--code')
		.addClass('success');

	$('.order-pay__summa').addClass('active')
		.find('.order-pay__summa-line--sale')
		.addClass('active');
});

$('.order-pay__option').on('click', function() {

	var optionType = $(this).attr('data-type'),
		optionTitle = $(this).attr('data-title'),
		optionPrice = $(this).attr('data-price'),
		optionBtn = $(this).attr('data-btn');

	$('.order-pay__price span:first-child').text(optionTitle);
	$('.order-pay__price span:last-child').text(optionPrice);
	$('.order-btn').text(optionBtn);

	if ( optionType === "full" ) {}

	if ( optionType === "installment" ) {
		$('.order-pay__summa').addClass('active')
			.find('.order-pay__summa-line--alt')
			.addClass('active');
			$('.order-pay__i').addClass('active');
	} else {
		$('.order-pay__summa').find('.order-pay__summa-line--alt')
			.removeClass('active');
			$('.order-pay__i').removeClass('active');
	}
});



$('.order-select__text').on('click', function(e) {
	e.preventDefault();

	$(this).closest('.order-select')
		.toggleClass('active');
});

$(document).mouseup(function (e) {
	var boxArea = $('.order-select');
	if (boxArea.has(e.target).length === 0) {
		boxArea.removeClass('active');
	}
});

$('.order-select__item').on('click', function(e) {
	e.preventDefault();

	var itemVal = $(this).text();

	$(this).addClass('selected')
		.siblings()
		.removeClass('selected');

	$(this).closest('.order-select')
		.removeClass('active')
		.find('.order-select__text')
		.text(itemVal);
});


$('.order-pay__type .form__label--check').on('click', function() {

	if ( $(this).attr('data-type') === "download-file" ) {
		$('.order-pay__download').addClass('active');
	} else {
		$('.order-pay__download').removeClass('active');
	}
});


$('.order-pay__check .form__check').on('change', function() {

	if ( $(this).prop('checked') ) {
		$('.order-pay__download a, .order-btn').removeClass('disabled');
	} else {
		$('.order-pay__download a, .order-btn').addClass('disabled');
	}
});

orderStepsContInit();
$(window).on('resize', function() {
	orderStepsContInit();
});
function orderStepsContInit() {
	if ( $('.order-steps__controls').length ) {
		if ( $(window).width() < 768 ) {
			// добавить
			if ( !$('.order-steps--all').length ) {
				$('.order-steps__controls').append('<div class="order-steps--all">/' + $('.order-step').length + '</div>');
			}
		} else {
			// убрать
			if ( $('.order-steps--all').length ) {
				$('.order-steps--all').remove();
			}
		}
	}
}
orderUserInit();
$(window).on('resize', function() {
	orderUserInit();
});
function orderUserInit() {
	if ( $('.order-users__btn').length ) {
		if ( $(window).width() < 768 ) {
			if ( !$('.order-users__btn').hasClass('active') ) {
				$('.order-users__btn:first-child, .order-user:first-child').addClass('active');
			}
		} else {
			$('.order-users__btn, .order-user').removeClass('active');
		}
	}
}

$(document).on('click', '.order-users__btn', function(e) {
	e.preventDefault();

	$(this).addClass('active')
		.siblings()
		.removeClass('active');
	var btnIndex = $(this).index();

	$('.order-user').each(function() {
		if ( $(this).index() === btnIndex ) {
			$(this).addClass('active')
				.siblings()
				.removeClass('active');
		}
	});
});