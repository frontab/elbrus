$('.slider-img').slick({
	dots: false,
	arrows: true,
	infinite: true,
	slidesToShow: 1,
	slidesToScroll: 1,
	prevArrow: '<button><i class="icon icon-angle-left"></i></button>',
	nextArrow: '<button><i class="icon icon-angle-right"></i></button>'
});