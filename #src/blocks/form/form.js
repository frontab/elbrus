$('.form__select').styler({
	onFormStyled: function() {
	  $('.jq-selectbox__dropdown ul').scrollbar();
	}
});

phoneMaskInit();
function phoneMaskInit() {
	$('input[type="tel"]').mask("+7 (999) 999-99-99", {
		completed: function() {
	
			if ( $(this).closest('.order-steps__box') ) {
				orderBtnCheck($(this), true);
			}
		}
	});
}